\documentclass[portrait,a0paper]{baposter}
\usepackage{graphicx,amsmath,tabularx,enumitem}
\graphicspath{{images/}}
\setlist[itemize]{leftmargin=2.2ex, itemsep=-0.2ex}
\hyphenpenalty=5000
\renewcommand\refname{}

\begin{document}
    \begin{poster}{
        colspacing=1em,
        columns=3,
        headerColorOne=cyan!20!white,
        headerColorTwo=white!10!cyan,
        borderColor=cyan!40!white,
        textborder=faded,
        headerborder=open,
        headershape=roundedright,
        headershade=plain,
        boxshade=none,
        boxColorOne = cyan!20!white!90!black,
        background=none,
        headerheight=0.13\textheight
        }
        {
           \includegraphics[height=0.11\textheight]{st_andrews}
            }
        {\sc\Huge \textbf{Polarisation in photon} \\ \vspace{0.5ex} \textbf{Bose-Einstein condensates}}
        {
            \vspace{1ex} Ryan Moodie \\
            Supervisor: Jonathan Keeling \\
            \vspace{1ex} \large \textit{School of Physics and Astronomy, University of St Andrews}
            }
        {
           \includegraphics[height=0.11\textheight]{laidlaw2}
            }
        %%% Now define the boxes that make up the poster
        %%%---------------------------------------------------------------------------
        %%% Each box has a name and can be placed absolutely or relatively.
        %%% The only inconvenience is that you can only specify a relative position 
        %%% towards an already declared box. So if you have a box attached to the 
        %%% bottom, one to the top and a third one which should be inbetween, you 
        %%% have to specify the top and bottom boxes before you specify the middle 
        %%% box.
        \headerbox{Motivation}{name=_motivation,column=0,row=0,span=1}{
            Bose-Einstein condensation is an exotic state of matter in which a gas of bosons all occupy a single state, displaying quantum phenomena at macroscopic scales. It was recently achieved for the first time with photons, in a laser-pumped dye-filled microcavity at room temperature \cite{klaers10}. Experiments at Imperial following \cite{marelic14} indicate that the way light is polarised changes on condensation. While a photon condensation model exists \cite{kirton13}\cite{kirton15}\cite{keeling16}, there is no previous work regarding its polarisation. Thus to describe this phenomenon, the theory was developed to include polarisation.
            }
        \headerbox{Experiment description}{name=_system,column=0,span=1,below=_motivation}{
            System diagram (from \cite{kirton15}):
            \begin{center}
                \includegraphics[width=0.9\textwidth]{diagram2}     
            \end{center}
            \begin{tabularx}{\textwidth}{l l l}
                $\kappa$ & : & cavity photon loss \\
                $\Gamma(\delta_{a})$ & : & molecular absorption of photons \\
                $\Gamma(-\delta_{a})$ & : & molecular emission of photons \\
                $\Gamma_{\uparrow}$ & : & pumping \\
                $\Gamma_{\downarrow}$ & : & fluorescence \\
            \end{tabularx}
            }
        \headerbox{Original equations}{name=_original,column=0,below=_system,span=1}{
            The rate equations without polarisation are:
            \vspace{-1ex}
            \begin{multline}
                \frac{\partial}{\partial t} \, n_{a} = - \kappa \, n_{a} + \Gamma(-\delta_{a}) \, \Big( n_{a} + 1 \Big) \, N_{\uparrow} \\
                 - \Gamma(\delta_{a}) \, n_{a} \, \Big( N - N_{\uparrow} \Big)
                \label{eq:photonOriginal}
            \end{multline}
            \vspace{-1ex}
            \begin{multline}
                \frac{\partial}{\partial t} \, N_{\uparrow} = 
                - \, \Gamma_{\downarrow} \, N_{\uparrow} \, + \, \Gamma_{\uparrow} \Big( N - N_{\uparrow} \Big)
                + \, \sum_{a=0}^{\infty} g_{a} \bigg \{ \\ \Gamma(\delta_a) \, n_a \, \Big( N - N_{\uparrow} \Big) \, - \, \Gamma(-\delta_a) \Big( n_{a} + 1 \Big) \, N_{\uparrow} \, \bigg \}
                \label{eq:moleculeOriginal}
            \end{multline}
            \vspace{-1ex}
            \begin{tabularx}{\textwidth}{l l l}
                $n_{a}$ & : & $a^{th}$ photon mode population \\
                $N_{\uparrow}$ & : & molecular excited state population \\
                $g_{a}$ & : & degeneracy factor ($ = a + 1$) \\
                $N$ & : & total number of molecules \\
            \end{tabularx}
            }
        \headerbox{Polarisation extension}{name=_extension,column=0,below=_original,span=1,above=bottom}{
            The equations were recast to include the angular dependence of the molecule-photon interaction:
            \vspace{-4ex}
            \begin{itemize}
                \item $n_{a} \rightarrow n_{a}^{\sigma}$
                \item $N_{\uparrow} \rightarrow N_{\uparrow}(\theta, \phi)$
                \item introduce molecular angular diffusion term
            \end{itemize}
            \begin{tabularx}{\textwidth}{l l l l}
                with: & $\sigma$ & : & polarisation state \\
                 & $D$ & : & diffusion coefficient \\
                 & $\theta$ & : & polar angle \\
                 & $\phi$ & : & azimuthal angle \\
                 & $\chi$ & : & pump polarisation angle \\
            \end{tabularx}
            }
        \headerbox{Equations with polarisation}{name=_new,column=1,row=0,span=2}{
            \begin{equation}
                \beta^{\sigma}(\varphi) = 
                \begin{cases}
                    \cos^{2}(\varphi) & \text{for } \sigma = x \\
                    \sin^{2}(\varphi) & \text{for } \sigma = y
                \end{cases}
                \label{eq:beta}
            \end{equation}
            \vspace{-3ex}
            \begin{multline}
                \frac{\partial}{\partial t} \, n_{a}^{\sigma} = - \kappa \, n_{a}^{\sigma} 
                + ( n_{a}^{\sigma} + 1 ) \, \Gamma(-\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \\
                 - n_{a}^{\sigma} \, \Gamma(\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big)
                 \label{eq:photonAngle}
            \end{multline}
            \vspace{-4ex}
            \begin{multline}
                \frac{\partial}{\partial t} \, N_{\uparrow}(\theta, \phi) = \,
                - \, \Gamma_{\downarrow} \, N_{\uparrow}(\theta, \phi) \,
                + \, \sum_{\sigma=(x, y)} \Bigg\{ \, \beta^{\sigma}(\chi) \, \Gamma_{\uparrow} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big) \\ 
                - \, \sum_{a=0}^{\infty} \, g_{a} \Bigg[ \Gamma(-\delta_{a}) \, ( n_{a}^{\sigma} + 1 ) \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \,
                + \, \Gamma(\delta_{a}) \, n_{a}^{\sigma} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big) \Bigg] \Bigg\} \\ 
                + \, D \, \Bigg[ \frac{1}{\sin^{2}(\theta)} \, \frac{\partial^{2}}{\partial \phi^{2}} + \frac{1}{\sin(\theta)} \, \frac{\partial}{\partial \theta} \bigg( \sin(\theta) \, \frac{\partial}{\partial \theta} \bigg) \Bigg] \, N_{\uparrow}(\theta, \phi)
                \label{eq:moleculeAngle}
            \end{multline}
            \vspace{-4ex}
        }
        \headerbox{Solving the equations}{name=_solving, column=1, span=1, below=_new}{
            A Python program was written to solve the rate equations:
            \vspace{-1ex}
            \begin{itemize}
            \item Parameters were set as in \cite{keeling16}.
            \item $D$ was set matching experimental data \cite{bojarski96}.
            \item The equations were intractable in angular space, so $N_{\uparrow}(\theta, \phi)$ was expressed as a sum of spherical harmonics with coefficients $N_{l, m}$.
            \item To test the simulation, steady-state spectra were plotted and found to match expectations:
            \end{itemize}
            \vspace{-3ex}
            \begin{center}
            \includegraphics[width=0.75\textwidth]{report_population_distribution_0}
            \end{center}
            \vspace{-4ex}
            }
        \headerbox{Population behaviour}{name=_system, column=1, span=1, below=_solving}{
            \begin{center}
                \begin{tabularx}{\textwidth}{l l}
                    \hspace{-2.75ex} \includegraphics[width=0.505\textwidth]{report_pumping_response_photon_mode_0_crop}
                    &
                    \hspace{-2.75ex} \includegraphics[width=0.505\textwidth]{report_pumping_response_excited_state_1_crop} \\
                    \begin{minipage}[t]{0.45\textwidth}
                        \begin{flushleft}
                            A clear threshold is seen with exotic multiple mode condensation.
                        \end{flushleft}
                    \end{minipage}
                    &
                    \begin{minipage}[t]{0.45\textwidth}
                        \begin{flushleft}
                            Above threshold, $N_{0, 0}$ (related to $N_{\uparrow}$) clamps and higher orders (reflecting molecule orientation distribution) suppress.
                        \end{flushleft}
                    \end{minipage} \\
                \end{tabularx}
            \end{center}
            \vspace{-4ex}
            }
        \headerbox{Polarisation response}{name=_polarisation, column=2, span=1, below=_new}{
            \begin{flushleft}
                Polarisation degree:
                \vspace{-1em}
            \end{flushleft}
            \begin{center}
                \begin{tabularx}{\textwidth}{l l l}
                    pump & : & $P_{in}$ = $\cos(2 \chi)$ \\
                    cavity photons & : & $P_{out}$ = $\frac{x-y}{x+y}$ \\
                \end{tabularx}
                \includegraphics[width=0.8\textwidth]{report_2d_colour_map_0}
            \end{center}
            \vspace{-2em}
            \begin{flushleft}
                With respect to threshold,
            \end{flushleft}
            \vspace{-1em}
            \begin{tabularx}{\textwidth}{l l}
                Below: & Weakly polarised photon gas;\\
                 & spontaneous emission allows \\
                  & molecule rotation time. \\
                Just above: & Strongly polarised condensate\\
                 & as single condensation mode. \\
                Far above: & $P_{out}$ follows $P_{in}$; stimulated\\
                 & emission suppresses diffusion. \\
            \end{tabularx}
            \vspace{-2ex}
        }
        \headerbox{Future outlook}{name=_curious, column=1, span=1, below=_system, above=bottom}{
            \begin{center}
            \includegraphics[width=0.75\textwidth]{report_output_polarisation_degree_against_diffusion_0}
            \end{center}
            \vspace{-2ex}
            A project extension will be undertaken to investigate some curious results, such as $P_{out}$ remaining small for a fully polarised pump when molecular angular diffusion is suppressed.             
            }
        \headerbox{Summary}{name=_conc, column=2, span=1, below=_polarisation }{
            An existing theoretical model of photon Bose-Einstein condensation in a dye-filled microcavity was extended to include polarisation and a computational simulation of the system produced to investigate its polarisation behaviour. 

            The model generally predicts expected results with some interesting exotic behaviour, furthering our understanding of photon condensation and its potential application. Some unusual outcomes have motivated a project extension.
            \vspace{-4ex}
            }
        \headerbox{References}{name=_ref, column=2, span=1, above=bottom, below=_conc}{
            \footnotesize
            \bibliographystyle{plain}
            \bibliography{references}
            }
    \end{poster}
\end{document}
